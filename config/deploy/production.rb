############################################
# Setup Server
############################################

set :stage, :production
set :stage_url, "https://www.muchowitsch.de"
server "85.214.199.179", user: "www-data", roles: %w{web app db}, port: 53475
set :deploy_to, "/var/www/muchowitsch.de/production"

############################################
# Setup Git
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

#set :ssh_options, {
#    auth_methods: %w(password),
#    password: 'password',
#    user: 'username',
#}

#specify a specific temp dir if user is jailed to home
#set :tmp_dir, "/path/to/custom/tmp"
