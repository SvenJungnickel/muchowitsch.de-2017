############################################
# Slack Configuration
############################################

# Required
set :slack_subdomain, 'atista' # if your Slack subdomain is example.slack.com
set :slack_url, 'https://hooks.slack.com/services/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' # https://my.slack.com/services/new/incoming-webhook

# Optional
set :slack_channel, '#deployments'
set :slack_emoji, ':rocket:'
