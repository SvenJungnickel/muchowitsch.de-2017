<?php
/**
 * Plugin Name:			Ocean Extra Child
 * Plugin URI:			https://oceanwp.org/extension/ocean-extra/
 * Description:			Child Plugin of Ocean Extra to add some extra features like widgets, metaboxes, import/export and a panel to activate the premium extensions.
 * Version:				1.3.2
 * Author:				OceanWP
 * Author URI:			https://oceanwp.org/
 * Requires at least:	4.5.0
 * Tested up to:		4.8.1
 *
 * Text Domain: ocean-extra-child
 * Domain Path: /languages/
 *
 * @package Ocean_Extra_Child
 * @category Core
 * @author OceanWP
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Returns the main instance of Ocean_Extra_Child to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Ocean_Extra_Child
 */
function Ocean_Extra_Child() {
	return Ocean_Extra_Child::instance();
} // End Ocean_Extra_Child()

Ocean_Extra_Child();

/**
 * Main Ocean_Extra Class
 *
 * @class Ocean_Extra
 * @version	1.0.0
 * @since 1.0.0
 * @package	Ocean_Extra
 */
final class Ocean_Extra_Child {
	/**
	 * Ocean_Extra_Child The single instance of Ocean_Extra_Child.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The token.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $token;

	/**
	 * The version number.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $version;

	// Admin - Start
	/**
	 * The admin object.
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $admin;

	/**
	 * Constructor function.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function __construct( $widget_areas = array() ) {
		$this->token 			= 'ocean-extra-child';
		$this->plugin_url 		= plugin_dir_url( __FILE__ );
		$this->plugin_path 		= plugin_dir_path( __FILE__ );
		$this->version 			= '1.3.2';

		define( 'OEC_PATH', $this->plugin_path );

		// Load custom widgets
		add_action( 'widgets_init', array( $this, 'custom_widgets' ), 10 );
	}

	/**
	 * Main Ocean_Extra_Child Instance
	 *
	 * Ensures only one instance of Ocean_Extra_Child is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see Ocean_Extra_Child()
	 * @return Ocean_Extra_Child instance
	 */
	public static function instance() {
		if ( null === self::$_instance )
			self::$_instance = new self();
		return self::$_instance;
	} // End instance()

	/**
	 * Load the localisation file.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'ocean-extra-child', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
	}

	/**
	 * Installation.
	 * Runs on activation. Logs the version number and assigns a notice message to a WordPress option.
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function install() {
		$this->_log_version_number();
	}

	/**
	 * Log the plugin version number.
	 * @access  private
	 * @since   1.0.0
	 * @return  void
	 */
	private function _log_version_number() {
		// Log the version number.
		update_option( $this->token . '-version', $this->version );
	}

	/**
	 * Setup all the things.
	 * Only executes if OceanWP or a child theme using OceanWP as a parent is active and the extension specific filter returns true.
	 * @return void
	 */
	public function setup() {
		$theme = wp_get_theme();

		if ( 'OceanWP' == $theme->name || 'oceanwp' == $theme->template ) {
//			add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ), 999 );
		} else {
			add_action( 'admin_notices', array( $this, 'install_oceanwp_notice' ) );
		}
	}

	/**
	 * OceanWP install
	 * If the user activates the plugin while having a different parent theme active, prompt them to install OceanWP.
	 * @since   1.0.0
	 * @return  void
	 */
	public function install_oceanwp_notice() {
		echo '<div class="notice is-dismissible updated">
				<p>' . esc_html__( 'Ocean Extra Child requires that you use OceanWP as your parent theme.', 'ocean-extra-child' ) . ' <a href="https://oceanwp.org/">' . esc_html__( 'Install OceanWP Now', 'ocean-extra-child' ) . '</a></p>
			</div>';
	}

	/**
	 * Include flickr widget class
	 *
	 * @since   1.0.0
	 */
	public static function custom_widgets() {

		if ( ! version_compare( PHP_VERSION, '5.2', '>=' ) ) {
			return;
		}

		// Define array of custom widgets for the theme
		$widgets = apply_filters( 'ocean_custom_widgets', array(
			'recent-posts',
		) );

		// Loop through widgets and load their files
		if ( $widgets && is_array( $widgets ) ) {
			foreach ( $widgets as $widget ) {
				$file = OEC_PATH .'/includes/widgets/' . $widget .'.php';
				if ( file_exists ( $file ) ) {
					require_once( $file );
				}
			}
		}

	}

	/**
	 * Enqueue scripts
	 *
	 * @since   1.0.0
	 */
//	public function scripts() {
//
//		// Load main stylesheet
//		wp_enqueue_style( 'oe-widgets-style', plugins_url( '/assets/css/widgets.css', __FILE__ ) );
//
//		// If rtl
//		if ( is_RTL() ) {
//			wp_enqueue_style( 'oe-widgets-style-rtl', plugins_url( '/assets/css/rtl.css', __FILE__ ) );
//		}
//
//	}

} // End Class